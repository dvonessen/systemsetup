# Export to circumvent forking failure
# See: [__NSPlaceholderDate initialize] may have been in progress in
# another thread when fork() was called.
# This is an ansible workaround
if [ $(uname) = "Darwin" ]; then
    export OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES
fi
