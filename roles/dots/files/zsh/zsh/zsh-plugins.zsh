# Powerlevel10k theme
# https://github.com/romkatv/powerlevel10k
[[ -f ~/.zsh/themes/external/powerlevel10k/powerlevel10k.zsh-theme ]] && source ~/.zsh/themes/external/powerlevel10k/powerlevel10k.zsh-theme
# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ -f ~/.zsh/p10k.zsh ]] && source ~/.zsh/p10k.zsh


# https://github.com/zsh-users/zsh-syntax-highlighting
# Enables syntax highlighting while typing
[[ -f ~/.zsh/plugins/external/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ]] && source ~/.zsh/plugins/external/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# https://github.com/zsh-users/zsh-history-substring-search
# Search history by typing a substring into shell and hit UP or DOWN to search for term
# Musst be after zsh-syntax-highlighting.zsh to ensure syntax highlighting
[[ -f ~/.zsh/plugins/external/zsh-history-substring-search/zsh-history-substring-search.zsh ]] && source ~/.zsh/plugins/external/zsh-history-substring-search/zsh-history-substring-search.zsh

# make UP and DOWN usable in history-substring-search
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down

# https://github.com/zsh-users/zsh-autosuggestions
# Autosuggest while typing into the sehll
[[ -f ~/.zsh/plugins/external/zsh-autosuggestions/zsh-autosuggestions.zsh ]] && source ~/.zsh/plugins/external/zsh-autosuggestions/zsh-autosuggestions.zsh

# Source local zsh libraries
if [ -d ~/.zsh/lib ]
then
    for zsh_lib in $(ls ~/.zsh/lib/*.zsh 2> /dev/null)
    do
        source $zsh_lib
    done
fi

# Source local zsh plugins
if [ -d ~/.zsh/plugins ]
then
    for zsh_plugin in $(ls ~/.zsh/plugins/*.zsh 2> /dev/null)
    do
        source $zsh_plugin
    done
fi
