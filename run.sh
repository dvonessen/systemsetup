#!/bin/bash

if [ -n "$DEBUG" ]
then
  set -x
fi

# Get current dir
export PATH="$HOME/.local/bin:$PATH"
CURRENT_DIR=$(pwd)
SCRIPT_PATH=$(realpath $(dirname $0))

# install git if not present
[[ -n "$(command -v git)" ]] || sudo pacman -Sy git || sudo dnf -y install git

# Check if ansible is installed
if [ -n "$(command -v ansible-playbook)" ]
then
  echo "ansible-playbook command available."
else
  echo "ansible-playbook command unavailable."
  echo "Try to install ansible."
  echo "Install python3 pip."
  sudo pacman -Sy python-pip || sudo dnf -y install python3-pip
  pip3 install --user ansible ansible-lint
fi


# Change to script path
cd $SCRIPT_PATH

# Clone git yay ansible library
[[ -d "./library/ansible-yay" ]] || git clone https://github.com/mnussbaum/ansible-yay.git ./library/ansible-yay
ln -fs ansible-yay/yay ./library/yay

test -f $SCRIPT_PATH/secrets/become_password.txt && ANSIBLE_OPTS="$ANSIBLE_OPTS --become-password-file $SCRIPT_PATH/secrets/become_password.txt" || ANSIBLE_OPTS="$ANSIBLE_OPTS -K"
test -f $SCRIPT_PATH/secrets/vault_password.txt && ANSIBLE_OPTS="$ANSIBLE_OPTS --vault-password-file $SCRIPT_PATH/secrets/vault_password.txt" || ANSIBLE_OPTS="$ANSIBLE_OPTS --ask-vault-password"

echo "Execute ansible-playbook."
time ansible-playbook $@ --diff $ANSIBLE_OPTS $SCRIPT_PATH/main.yml 
cd $CURRENT_PATH

